## Docker Session

### docker pull

```bash
docker pull <image-name>
```



### docker build

```bash
docker build -t <specify-image-name> <dockerfile-location>
```




### docker run

```bash
docker run python3.7 "python -c print('Hello')"
```



### docker exec

```bash
docker exec -it -e <environment> <container-name> <command>
```



### Spring Dockerfile

```dockerfile
FROM maven:3-jdk-8-alpine as builder
EXPOSE 8080
WORKDIR /tmp
ADD pom.xml .
RUN mvn -U dependency:resolve-plugins dependency:go-offline
ADD src src
RUN ls -la
RUN mvn clean compile
RUN ls -la
RUN mvn verify -Dmaven.test.skip=true

FROM openjdk:alpine
RUN mkdir -p /app
COPY --from=builder /tmp/target/backend-1.0.0-SNAPSHOT.jar /app/backend-1.0.0-SNAPSHOT.jar 
WORKDIR /app
RUN ls -la
CMD ["java", "-jar", "backend-1.0.0-SNAPSHOT.jar"]
```

