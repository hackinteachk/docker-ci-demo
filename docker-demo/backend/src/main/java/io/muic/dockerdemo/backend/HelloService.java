package io.muic.dockerdemo.backend;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HelloService {

    private final HelloDao dao;

    @Autowired
    public HelloService(HelloDao dao){
        this.dao = dao;
    }


    public List<HelloEntity> getMessages(){
        return dao.findAll();
    }

    public HelloEntity createMessage(String msg){
        return dao.createMessage(msg);
    }

}
