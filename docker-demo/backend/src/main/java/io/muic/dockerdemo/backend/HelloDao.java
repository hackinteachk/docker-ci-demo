package io.muic.dockerdemo.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HelloDao  {

    @Autowired
    private HelloRepository helloRepository;

    public List<HelloEntity> findAll() {
        return helloRepository.findAll(new Sort(Sort.Direction.DESC, "modified"));
    }

    public HelloEntity createMessage(String message) {
        HelloEntity entity = new HelloEntity();
        entity.setMessage(message);
        return helloRepository.save(entity);
    }
}
