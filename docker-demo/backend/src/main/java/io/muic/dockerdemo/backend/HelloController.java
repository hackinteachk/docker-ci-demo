package io.muic.dockerdemo.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")
public class HelloController {

    private final HelloService service;

    @Autowired
    public HelloController(HelloService service){
        this.service = service;
    }

    @GetMapping
    public List<HelloEntity> getAllMessage(){
        return service.getMessages();
    }

    @PostMapping
    public ResponseEntity<HelloEntity> create(@RequestParam String message){
        if (message.trim().isEmpty()) return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        return ResponseEntity.ok(service.createMessage(message));
    }
}
