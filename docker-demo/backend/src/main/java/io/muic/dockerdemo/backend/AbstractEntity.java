package io.muic.dockerdemo.backend;


import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
public abstract class AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime created;
    private LocalDateTime modified;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getModified() {
        return modified;
    }

    public void setModified(LocalDateTime modified) {
        this.modified = modified;
    }

    @PrePersist
    public void pre(){
        LocalDateTime now = LocalDateTime.now();
        this.setCreated(now);
        this.setModified(now);
    }

    @PreUpdate
    public void update(){
        LocalDateTime now = LocalDateTime.now();
        this.setModified(now);
    }
}
