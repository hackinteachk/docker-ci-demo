package io.muic.dockerdemo.backend;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.stream.Stream;

public interface HelloRepository extends JpaRepository<HelloEntity, Long> {
}
