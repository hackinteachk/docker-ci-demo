package io.muic.dockerdemo.backend;

import javax.persistence.Entity;

@Entity
public class HelloEntity extends AbstractEntity {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
