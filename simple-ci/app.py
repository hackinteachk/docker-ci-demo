from flask import Flask, jsonify, request

app = Flask(__name__)
#cors = CORS(app)
#app.config['CORS_HEADERS'] = '*'
#logging.basicConfig(level=logging.DEBUG)

@app.route('/helloworld',methods=["GET"])
def helloworld():
    ret = {'data':'hello, from ci-demo'}
    return jsonify(ret), 200

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000, threaded=False)
