from requests import get

def test_hello():
    resp = get('http://localhost:5000/helloworld')
    ret = resp.json()
    assert ret['data'] == 'hello, from ci-demo'
